//
//  ViewController.m
//  LocalChatClient
//
//  Created by Leonardo Saragiotto on 6/21/17.
//  Copyright © 2017 Leonardo Saragiotto. All rights reserved.
//

#import "ViewController.h"
#import "ChatViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self setTitle:@"Chat"];
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"startChat"]) {
        ChatViewController *chatVC = (ChatViewController *)segue.destinationViewController;
        chatVC.apelido = self.apelido.text;
        chatVC.chatURL = self.chatURL.text;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
