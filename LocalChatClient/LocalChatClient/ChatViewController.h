//
//  ChatViewController.h
//  LocalChatClient
//
//  Created by user on 22/06/17.
//  Copyright © 2017 Leonardo Saragiotto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ChatViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) NSString *apelido;
@property (nonatomic, strong) NSString *chatURL;
@property (weak, nonatomic) IBOutlet UITextView *mensagens;
@property (weak, nonatomic) IBOutlet UITextField *inputText;

@end
