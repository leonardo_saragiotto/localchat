//
//  ChatViewController.m
//  LocalChatClient
//
//  Created by user on 22/06/17.
//  Copyright © 2017 Leonardo Saragiotto. All rights reserved.
//

#import "ChatViewController.h"
#import <SocketIO/SocketIO-Swift.h>

@interface ChatViewController ()

@property (nonatomic, strong) SocketIOClient* socket;

@end

@implementation ChatViewController 

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setTitle:self.apelido];
    
    NSURL* url = [[NSURL alloc] initWithString:self.chatURL];
    
    self.socket = [[SocketIOClient alloc] initWithSocketURL:url config:@{@"log": @YES}];
    
    self.mensagens.layer.borderColor = [UIColor blackColor].CGColor;
    self.mensagens.layer.borderWidth = 1.0f;
    self.mensagens.layer.cornerRadius = 5.0f;
    
    [self.inputText becomeFirstResponder];
    self.inputText.delegate = self;
    
    [self.socket on:@"connect" callback:^(NSArray* data, SocketAckEmitter* ack) {
        NSLog(@"socket connected");
        
        [self emitMessage:@"estou connectado!"];
    }];
    
    [self.socket on:@"chat message" callback:^(NSArray* data, SocketAckEmitter* ack) {
        
        NSString *last = @"\n-----------------------";
        
        self.mensagens.text = [self.mensagens.text stringByReplacingOccurrencesOfString:last withString:@""];
        
        self.mensagens.text = [NSString stringWithFormat:@"%@\n%@%@", self.mensagens.text, data.firstObject, last];
        NSLog(@"data - %@", data);
    }];
    
    [self.socket onAny:^(SocketAnyEvent * _Nonnull event) {
        NSLog(@"event received!");
    }];
    
    [self.socket connect];
}

- (void)emitMessage:(NSString *)message {
    NSString *mensagemCompleta = [NSString stringWithFormat:@"%@ diz: %@", self.apelido, message];
    
    [self.socket emit:@"chat message" with:@[mensagemCompleta]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.socket disconnect];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    if ([self.inputText.text isEqualToString:@""]) {
        return NO;
    }
    
    [self emitMessage:self.inputText.text];
    self.inputText.text = @"";
    
    if(self.mensagens.bounds.size.height < self.mensagens.contentSize.height) {
        
        CGFloat newY = self.mensagens.contentSize.height - self.mensagens.bounds.size.height;
        
        
        [self.mensagens scrollRectToVisible:CGRectMake(0.0f, newY, self.mensagens.bounds.size.width, self.mensagens.bounds.size.height) animated:YES];
    }
    
    return NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
