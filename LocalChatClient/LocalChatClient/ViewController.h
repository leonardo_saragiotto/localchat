//
//  ViewController.h
//  LocalChatClient
//
//  Created by Leonardo Saragiotto on 6/21/17.
//  Copyright © 2017 Leonardo Saragiotto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *apelido;
@property (weak, nonatomic) IBOutlet UITextField *chatURL;

@end

